//
//  Protocal.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation

protocol APIServiceDelegate {
    func APICall(apiType:String, url: String,method : HTTPMethod,param : [String: Any]?, success:@escaping (Data) -> (),failure:@escaping (String) -> ())
}
