//
//  Extenstion.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation
import UIKit

extension String {
    
    static let kEmpty            = ""
    static let kOK               = "OK"
    static let kAlert            = "Alert!"
    static let kAvatar           = "Avatar"
    static let kPhoto            = "Photo"
    static let kCamera           = "Camera"
    static let kCancel           = "Cancel"
    static let kWarning          = "Warning"
    static let kServerError      = "Server Error"
    static let kKolkata          = "Kolkata"
    static let kEmail            = "Email"
    static let kPassword         = "Password"
    static let kConfirmPassword  = "Confirm Password"
    static let kUserName         = "User Name"
    static let kAShortBio        = "A Short Bio"
    static let kJSON             = "json"
    static let kCITY_LIST        = "cityList"
    static let kCITY_NAME        = "CITY_NAME"
    static let kPROFILE_UPLOADED = "PROFILE_UPLOADED"
    
}

extension String {
    //MARK: - IS VALID EMAIL
    func isValidEmail() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
