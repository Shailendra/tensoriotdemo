//
//  Extension+View.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation
import UIKit

extension UIView {
    
    //MARK: - DROP THE SHADOW
    func dropShadow() {
        self.layer.cornerRadius  = 4
        self.layer.shadowColor   = UIColor.black.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset  = CGSize.zero
        self.layer.shadowRadius  = 5
    }
    
    //MARK: - DROP THE SHADOW WITH PARAM
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds      = false
        layer.shadowColor        = color.cgColor
        layer.shadowOpacity      = opacity
        layer.shadowOffset       = offSet
        layer.shadowRadius       = radius
        layer.shadowPath         = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize    = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
