//
//  Extension+VC.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation
import UIKit

extension UIViewController {
    
    //MARK: - MOVE TO PREVIOUS SCREEN
    func POP(){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - SHOW THE ALERT
    func showAlert(isRegister : Bool = false, title:String,message:String) {
        let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: .kOK, style: UIAlertAction.Style.destructive, handler: { _ in
            if isRegister {
                self.POP()
            }
        }))
        DispatchQueue.main.async {
            self.present(alert, animated: false, completion: nil)
        }
    }
    
    //MARK: - MOVE THE PROFILE SCREEN
    func MoveToProfileScreen(userName: String, shortBio:String, imageUrl:String){
        let storyboard = UIStoryboard(name: StoryboardName.kMain, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: IdentifireName.kProfileVC) as! ProfileVC
        vc.userName = userName
        vc.shortBio = shortBio
        vc.imageUrl = imageUrl
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
