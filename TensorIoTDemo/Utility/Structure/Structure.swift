//
//  Structure.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation


struct IdentifireName {
    static let kWeatherVC   = "WeatherVC"
    static let kProfileVC   = "ProfileVC"
}

struct StoryboardName {
    static let kMain   = "Main"
}

struct CellName {
    static let kCityCell            = "CityCell"
    static let kEmailCell           = "EmailCell"
    static let kPasswordCell        = "PasswordCell"
    static let kConfirmPasswordCell = "ConfirmPasswordCell"
    static let kUserNameCell        = "UserNameCell"
    static let kBioCell             = "BioCell"
}

struct ImageName {
    static let kAvator   = "ic-avator"
}

