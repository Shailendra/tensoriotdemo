//
//  Constant.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation

//MARK: - API Keys
let kAPIKey = "5e5bd501f7e5d4e549b00a4d2721e497"

// Basic
let kContentTypeKey    = "Content-Type"
let kContentTypeValue  = "application/json"
let kAccept            = "Accept"
let kAuthorizationKey  = "Authorization"

//===========================================
// MARK:- BASE URL
//===========================================

let kCityList = "https://www.universal-tutorial.com/api/cities/"
let kWeather  = "https://api.openweathermap.org/data/2.5/weather?q="
