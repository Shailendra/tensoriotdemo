//
//  APIServices.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import SwiftUI

class APIServices : APIServiceDelegate {
    
    //MARK: - PROPETIES
    let parseHandler: ParseHelper
    
    //MARK: - DEGINATED INIT WITH PARAM
    init(parseHandler : ParseHelper){
        self.parseHandler = parseHandler
    }
    
    //MARK: - API CALLING WITH PARAM
    func APICall(apiType:String, url: String,method : HTTPMethod = .GET,param : [String: Any]?, success:@escaping (Data) -> (),failure:@escaping (String) -> ()) {
        
        let session = URLSession.shared
        var request = URLRequest(url: URL(string:url)!)
        request.httpMethod = method.rawValue
        request.addValue(kContentTypeValue, forHTTPHeaderField: kContentTypeKey)
        request.addValue(kContentTypeValue, forHTTPHeaderField: kAccept)
        if apiType == "CITY"{
            request.addValue("Bearer \(CityToken)", forHTTPHeaderField: kAuthorizationKey)
        }
        if param != nil {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: param as Any, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
                return
            }
        }
        let task = session.dataTask(with: request) { data, response, error in
            if let error = error {
                print("Post Request Error: \(error.localizedDescription)")
                return
            }
            let httpResponse = response as? HTTPURLResponse
            if (200...299).contains(httpResponse!.statusCode) {
                
            }else if (400...500).contains(httpResponse!.statusCode){
                failure(Message.shared.pleaseTryAgain)
                return
            }
            
            guard data != nil else {
                print("nil Data received from the server")
                return
            }
            if let data = data {
                success(data)
            }
        }
        task.resume()
    }
}

//MARK: - SOLID RULE
class ParseHelper{
    
//    func parse(data: Data,success:@escaping (Data) -> ()) {
//        do {
//            if let jsonResponse = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
//                success(responseData)
//            } else {
//                print("data maybe corrupted or in wrong format")
//                throw URLError(.badServerResponse)
//            }
//        } catch let error {
//            print(error.localizedDescription)
//        }
//    }
}


