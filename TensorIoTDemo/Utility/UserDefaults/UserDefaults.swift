//
//  UserDefaultManager.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation

func getCityName()->String{
    
    if let getCityName = UserDefaults.standard.value(forKey: .kCITY_NAME) as? String {
        return getCityName
    }
    return "Kolkata"
}

//MARK: - REMOVE ALL KEYS
func removeAllKeys(){
    UserDefaults.standard.removeObject(forKey: .kCITY_NAME)
}
