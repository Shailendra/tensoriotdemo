//
//  Message.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation

class Message {
    
   static let shared = Message()
    
    let enterEmailID         = "Please enter email id"
    let validEmailID         = "Please enter valid email id"
    let enterPassword        = "Please enter password"
    let selectAnOption       = "Please Select an Option"
    let dontHaveCamera       = "You don't have camera"
    let somethingWentWrong   = "Something went wrong"
    let pleaseTryAgain       = "Something went wrong please try again!!"
    let uploadProfileImage   = "Please upload profile image"
    let enterConfirmPassword = "Please enter confirm password"
    let samePassword         = "Password and confirm password should be same"
    let enterUserName        = "Please enter user name"
    let enterShortBio        = "Please enter short bio"
    let failedToPush         = "Failed to push image to store"
    let registerSuccess      = "User register successfully done\n Please login"
    
    
}
