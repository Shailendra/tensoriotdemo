//
//  CityVM.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation

class CityVM  {
    
    //MARK: - PROPERTIES
    private var apiService: APIServiceDelegate
    var reloadTableView: (() -> Void)?
    
    init(apiService: APIServiceDelegate = APIServices(parseHandler: ParseHelper())) {
        self.apiService = apiService
    }
    var cityModel = [CityCodable]() {
        didSet {
            reloadTableView?()
        }
    }
    
    //MARK: - GET CITY LIST
    public func getCityList(errorMessage:@escaping (String) -> ()){
        
        if let path = Bundle.main.path(forResource: .kCITY_LIST, ofType: .kJSON) {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let cities = try JSONDecoder().decode([CityCodable].self, from: data)
                fetchData(cityList: cities)
            } catch {
                errorMessage(Message.shared.somethingWentWrong)
            }
        }
    }
}

extension CityVM {
    
    //MARK: - FETCH CITY LIST DATA
    func fetchData(cityList : [CityCodable]){
        self.cityModel = cityList.sorted(by: { City1, City2 in
            City1.city_name ?? .kEmpty < City2.city_name ?? .kEmpty
        })
    }
}
