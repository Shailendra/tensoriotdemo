//
//  CityListVC.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import UIKit

class CityListVC: UIViewController {

    //MARK: - PROPERTIES
    @IBOutlet var cityTableView : UITableView!
    var viewModel = {
        CityVM()
    }()
    
    //MARK: - LIFE CYCLE OF VIEW CONTROLLER
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cityTableView.backgroundColor = UIColor.clear
        let views = UIView()
        views.backgroundColor = UIColor.clear
        self.cityTableView.backgroundView = views
        self.initViewModel()
    }
    
    //MARK: - INIT VIEW MODEL CALL AND FETCH THE USERS LIST AND DISPLAY THE TABLE VIEW
    private func initViewModel(){
        self.viewModel.getCityList { error in
            self.showAlert(title: .kAlert, message: error)
        }
        self.viewModel.reloadTableView = { [weak self] in
            DispatchQueue.main.async {
                self?.cityTableView.reloadData()
            }
        }
    }
    
    //MARK: - MOVE TO PRIVIOUS PAGE
    @IBAction func BackAction(){
        self.POP()
    }
}
