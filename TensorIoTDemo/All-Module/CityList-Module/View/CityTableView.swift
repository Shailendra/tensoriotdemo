//
//  CityTableView.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation
import UIKit


//===============================================================
//MARK: - UITableViewDelegate,UITableViewDataSource
//===============================================================
extension CityListVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewModel.cityModel.count > 0 {
          return self.viewModel.cityModel.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:CityCell.identifier, for: indexPath) as!
        CityCell
        if self.viewModel.cityModel.count > 0 {
            cell.CityLabel.text = "\(self.viewModel.cityModel[indexPath.row].city_name ?? .kEmpty)"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.viewModel.cityModel.count > 0 {
            UserDefaults.standard.set("\(self.viewModel.cityModel[indexPath.row].city_name ?? .kEmpty)", forKey: .kCITY_NAME)
            self.navigationController?.popViewController(animated: true)
        }
    }
}
