//
//  CityM.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation

struct CityCodable : Codable {
    
    let city_name : String?
    
    enum CodingKeys: String, CodingKey {
        
        case city_name = "city_name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.city_name = try values.decodeIfPresent(String.self, forKey: .city_name)
    }
}
