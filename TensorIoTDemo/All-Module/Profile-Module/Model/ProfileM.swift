//
//  ProfileM.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation

struct WeatherCodable : Codable {
    
    let coord      : Coord?
    let weather    : [Weather]?
    let base       : String?
    let main       : Main?
    let visibility : Int?
    let wind       : Wind?
    let clouds     : Clouds?
    let dt         : Int?
    let sys        : Sys?
    let timezone   : Int?
    let id         : Int?
    let name       : String?
    let cod        : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case coord      = "coord"
        case weather    = "weather"
        case base       = "base"
        case main       = "main"
        case visibility = "visibility"
        case wind       = "wind"
        case clouds     = "clouds"
        case dt         = "dt"
        case sys        = "sys"
        case timezone   = "timezone"
        case id         = "id"
        case name       = "name"
        case cod        = "cod"
    }
    
    init(from decoder: Decoder) throws {
        
        let values      = try decoder.container(keyedBy: CodingKeys.self)
        self.coord      = try values.decodeIfPresent(Coord.self, forKey: .coord)
        self.weather    = try values.decodeIfPresent([Weather].self, forKey: .weather)
        self.base       = try values.decodeIfPresent(String.self, forKey: .base)
        self.main       = try values.decodeIfPresent(Main.self, forKey: .main)
        self.visibility = try values.decodeIfPresent(Int.self, forKey: .visibility)
        self.wind       = try values.decodeIfPresent(Wind.self, forKey: .wind)
        self.clouds     = try values.decodeIfPresent(Clouds.self, forKey: .clouds)
        self.dt         = try values.decodeIfPresent(Int.self, forKey: .dt)
        self.sys        = try values.decodeIfPresent(Sys.self, forKey: .sys)
        self.timezone   = try values.decodeIfPresent(Int.self, forKey: .timezone)
        self.id         = try values.decodeIfPresent(Int.self, forKey: .id)
        self.name       = try values.decodeIfPresent(String.self, forKey: .name)
        self.cod        = try values.decodeIfPresent(Int.self, forKey: .cod)
    }
}

struct Clouds : Codable {
    
    let all : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case all = "all"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.all        = try values.decodeIfPresent(Int.self, forKey: .all)
    }
}

struct Coord : Codable {
    
    let lon : Double?
    let lat : Double?
    
    enum CodingKeys: String, CodingKey {
        
        case lon = "lon"
        case lat = "lat"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.lon   = try values.decodeIfPresent(Double.self, forKey: .lon)
        self.lat   = try values.decodeIfPresent(Double.self, forKey: .lat)
    }
}

struct Main : Codable {
    
    let temp       : Double?
    let feels_like : Double?
    let temp_min   : Double?
    let temp_max   : Double?
    let pressure   : Int?
    let humidity   : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case temp       = "temp"
        case feels_like = "feels_like"
        case temp_min   = "temp_min"
        case temp_max   = "temp_max"
        case pressure   = "pressure"
        case humidity   = "humidity"
    }
    
    init(from decoder: Decoder) throws {
        
        let values      = try decoder.container(keyedBy: CodingKeys.self)
        self.temp       = try values.decodeIfPresent(Double.self, forKey: .temp)
        self.feels_like = try values.decodeIfPresent(Double.self, forKey: .feels_like)
        self.temp_min   = try values.decodeIfPresent(Double.self, forKey: .temp_min)
        self.temp_max   = try values.decodeIfPresent(Double.self, forKey: .temp_max)
        self.pressure   = try values.decodeIfPresent(Int.self, forKey: .pressure)
        self.humidity   = try values.decodeIfPresent(Int.self, forKey: .humidity)
    }
}

struct Sys : Codable {
    
    let type    : Int?
    let id      : Int?
    let country : String?
    let sunrise : Int?
    let sunset  : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case type    = "type"
        case id      = "id"
        case country = "country"
        case sunrise = "sunrise"
        case sunset  = "sunset"
    }
    
    init(from decoder: Decoder) throws {
        
        let values   = try decoder.container(keyedBy: CodingKeys.self)
        self.type    = try values.decodeIfPresent(Int.self, forKey: .type)
        self.id      = try values.decodeIfPresent(Int.self, forKey: .id)
        self.country = try values.decodeIfPresent(String.self, forKey: .country)
        self.sunrise = try values.decodeIfPresent(Int.self, forKey: .sunrise)
        self.sunset  = try values.decodeIfPresent(Int.self, forKey: .sunset)
    }
}

struct Weather : Codable {
    
    let id          : Int?
    let main        : String?
    let description : String?
    let icon        : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id          = "id"
        case main        = "main"
        case description = "description"
        case icon        = "icon"
    }
    
    init(from decoder: Decoder) throws {
        let values       = try decoder.container(keyedBy: CodingKeys.self)
        self.id          = try values.decodeIfPresent(Int.self, forKey: .id)
        self.main        = try values.decodeIfPresent(String.self, forKey: .main)
        self.description = try values.decodeIfPresent(String.self, forKey: .description)
        self.icon        = try values.decodeIfPresent(String.self, forKey: .icon)
    }
}

struct Wind : Codable {
    
    let speed : Double?
    let deg   : Int?
    
    enum CodingKeys: String, CodingKey {
        
        case speed = "speed"
        case deg   = "deg"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.speed = try values.decodeIfPresent(Double.self, forKey: .speed)
        self.deg   = try values.decodeIfPresent(Int.self, forKey: .deg)
    }
}
