//
//  WeatherCell.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import UIKit

class WeatherCell: UITableViewCell {

    //MARK: - PROPERTIES
    @IBOutlet weak var CityNameLabel    : UILabel!
    @IBOutlet weak var CloudsLabel      : UILabel!
    @IBOutlet weak var LatLabel         : UILabel!
    @IBOutlet weak var LongLabel        : UILabel!
    @IBOutlet weak var DegLabel         : UILabel!
    @IBOutlet weak var SpeedLabel       : UILabel!
    @IBOutlet weak var HimunityLabel    : UILabel!
    @IBOutlet weak var PressureLabel    : UILabel!
    @IBOutlet weak var Tempreture       : UILabel!
    @IBOutlet weak var TempMaxLabel     : UILabel!
    @IBOutlet weak var TempMinLabel     : UILabel!
    @IBOutlet weak var CountryLabel     : UILabel!
    @IBOutlet weak var SunriseLabel     : UILabel!
    @IBOutlet weak var SunsetLabel      : UILabel!
    @IBOutlet weak var WeatherLabel     : UILabel!
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    var viewModel : ProfileVM!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(){
        self.CityNameLabel.text = self.viewModel.wearherModel.name ?? .kEmpty
        self.CloudsLabel.text   = "\(self.viewModel.wearherModel.clouds?.all ?? 0)"
        self.LatLabel.text      = "\(self.viewModel.wearherModel.coord?.lat ?? 0)"
        self.LongLabel.text     = "\(self.viewModel.wearherModel.coord?.lon ?? 0)"
        self.HimunityLabel.text = "\(self.viewModel.wearherModel.main?.humidity ?? 0)"
        self.PressureLabel.text = "\(self.viewModel.wearherModel.main?.pressure ?? 0)"
        self.Tempreture.text    = "\(self.viewModel.wearherModel.main?.temp ?? 0)"
        self.TempMaxLabel.text  = "\(self.viewModel.wearherModel.main?.temp_max ?? 0)"
        self.TempMinLabel.text  = "\(self.viewModel.wearherModel.main?.temp_min ?? 0)"
        self.CountryLabel.text  = self.viewModel.wearherModel.sys?.country ?? .kEmpty
        self.SunriseLabel.text  = "\(self.viewModel.wearherModel.sys?.sunrise ?? 0)"
        self.SunsetLabel.text   = "\(self.viewModel.wearherModel.sys?.sunset ?? 0)"
        self.WeatherLabel.text  = self.viewModel.wearherModel.weather?[0].main ?? .kEmpty
        self.DegLabel.text      = "\(self.viewModel.wearherModel.wind?.deg ?? 0)"
        self.SpeedLabel.text    = "\(self.viewModel.wearherModel.wind?.speed ?? 0)"
    }
}
