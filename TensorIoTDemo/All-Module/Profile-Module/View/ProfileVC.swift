//
//  ProfileVC.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import UIKit
import Kingfisher

class ProfileVC: UIViewController {

    //MARK: - PROPERTIES
    @IBOutlet weak var profileTableView : UITableView!
    @IBOutlet weak var profileImageView : UIImageView!
    @IBOutlet weak var userNameLabel    : UILabel!
    @IBOutlet weak var shortBioLabel    : UILabel!
//    @IBOutlet weak var CityNameLabel    : UILabel!
//    @IBOutlet weak var CloudsLabel      : UILabel!
//    @IBOutlet weak var LatLabel         : UILabel!
//    @IBOutlet weak var LongLabel        : UILabel!
//    @IBOutlet weak var DegLabel         : UILabel!
//    @IBOutlet weak var SpeedLabel       : UILabel!
//    @IBOutlet weak var HimunityLabel    : UILabel!
//    @IBOutlet weak var PressureLabel    : UILabel!
//    @IBOutlet weak var Tempreture       : UILabel!
//    @IBOutlet weak var TempMaxLabel     : UILabel!
//    @IBOutlet weak var TempMinLabel     : UILabel!
//    @IBOutlet weak var CountryLabel     : UILabel!
//    @IBOutlet weak var SunriseLabel     : UILabel!
//    @IBOutlet weak var SunsetLabel      : UILabel!
//    @IBOutlet weak var WeatherLabel     : UILabel!
    var userName                        : String!
    var shortBio                        : String!
    var imageUrl                        : String!
    var viewModel = {
        ProfileVM()
    }()
  
    
    
    //MARK: - LIFE CYCLE OF VIEW CONTROLLER
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - LIFE CYCLE OF VIEW CONTROLLER
    override func viewWillAppear(_ animated: Bool) {
        self.userNameLabel.text = self.userName
        self.shortBioLabel.text = self.shortBio
        self.profileImageView.kf.setImage(with: URL(string: self.imageUrl),placeholder: UIImage(named: ImageName.kAvator))
      //  self.ClearText()
        self.initViewModel()
    }
    
    //MARK: - INIT VIEW MODEL CALL AND FETCH THE WEATHER DETAILS AND DISPLAY THE DATA
    private func initViewModel(){
        self.viewModel.getWeatherDetails(cityName: getCityName()) { error in
            self.showAlert(title: .kServerError, message: Message.shared.somethingWentWrong)
        }
        self.viewModel.responseData = { [self] in
            DispatchQueue.main.async {
                self.profileTableView.reloadData()
//                self.CityNameLabel.text = self.viewModel.wearherModel.name ?? .kEmpty
//                self.CloudsLabel.text   = "\(self.viewModel.wearherModel.clouds?.all ?? 0)"
//                self.LatLabel.text      = "\(self.viewModel.wearherModel.coord?.lat ?? 0)"
//                self.LongLabel.text     = "\(self.viewModel.wearherModel.coord?.lon ?? 0)"
//                self.HimunityLabel.text = "\(self.viewModel.wearherModel.main?.humidity ?? 0)"
//                self.PressureLabel.text = "\(self.viewModel.wearherModel.main?.pressure ?? 0)"
//                self.Tempreture.text    = "\(self.viewModel.wearherModel.main?.temp ?? 0)"
//                self.TempMaxLabel.text  = "\(self.viewModel.wearherModel.main?.temp_max ?? 0)"
//                self.TempMinLabel.text  = "\(self.viewModel.wearherModel.main?.temp_min ?? 0)"
//                self.CountryLabel.text  = self.viewModel.wearherModel.sys?.country ?? .kEmpty
//                self.SunriseLabel.text  = "\(self.viewModel.wearherModel.sys?.sunrise ?? 0)"
//                self.SunsetLabel.text   = "\(self.viewModel.wearherModel.sys?.sunset ?? 0)"
//                self.WeatherLabel.text  = self.viewModel.wearherModel.weather?[0].main ?? .kEmpty
//                self.DegLabel.text      = "\(self.viewModel.wearherModel.wind?.deg ?? 0)"
//                self.SpeedLabel.text    = "\(self.viewModel.wearherModel.wind?.speed ?? 0)"
            }
        }
    }
    
//    //MARK: - CLEAR THE DATE OF PREVIOUS DISPLAY
//    func ClearText(){
//        self.CityNameLabel.text = .kEmpty
//        self.CloudsLabel.text   = .kEmpty
//        self.LatLabel.text      = .kEmpty
//        self.LongLabel.text     = .kEmpty
//        self.DegLabel.text      = .kEmpty
//        self.SpeedLabel.text    = .kEmpty
//        self.HimunityLabel.text = .kEmpty
//        self.PressureLabel.text = .kEmpty
//        self.Tempreture.text    = .kEmpty
//        self.TempMaxLabel.text  = .kEmpty
//        self.TempMinLabel.text  = .kEmpty
//        self.CountryLabel.text  = .kEmpty
//        self.SunriseLabel.text  = .kEmpty
//        self.SunsetLabel.text   = .kEmpty
//        self.WeatherLabel.text  = .kEmpty
//    }
    
    //MARK: - MOVE TO PREVIOUSE SCREEN
    @IBAction func backButtonAction(){
        self.POP()
    }
}
