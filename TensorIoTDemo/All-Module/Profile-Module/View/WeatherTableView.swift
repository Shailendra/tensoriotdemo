//
//  ProfileTableView.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation
import UIKit


//===============================================================
//MARK: - UITableViewDelegate,UITableViewDataSource
//===============================================================
extension ProfileVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.wearherModel == nil ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:WeatherCell.identifier, for: indexPath) as!
        WeatherCell
        if self.viewModel.wearherModel != nil {
            cell.viewModel = self.viewModel
            cell.configureCell()
        }
        return cell
    }
}
