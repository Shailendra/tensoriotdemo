//
//  ProfileVM.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation

class ProfileVM  {
    
    //MARK: - PROPERTIES
    private var apiService: APIServiceDelegate
    var responseData: (() -> Void)?
    var wearherModel : WeatherCodable!
    
    
    //MARK: - INITIALIZATION WITH PARAM
    init(apiService: APIServiceDelegate = APIServices(parseHandler: ParseHelper())) {
        self.apiService = apiService
    }
   
    
    //MARK: - GET WEATHER DETAILS
    public func getWeatherDetails(cityName: String, errorMessage:@escaping (String) -> ()){
        self.apiService.APICall(apiType : "WEATHER", url: kWeather + cityName.replacingOccurrences(of: " ", with: "%20") + "&appid=\(kAPIKey)", method: .GET, param: nil) { [self] response in
            do {
                let cities = try JSONDecoder().decode(WeatherCodable.self, from: response)
                fetchData(WeatherDetails: cities)
            } catch {
                errorMessage(error.localizedDescription)
            }
        } failure: { error in
            errorMessage(error.debugDescription)
        }
    }
}

extension ProfileVM {
    
    //MARK: - FETCH WEATHER DETAILS DATA
    func fetchData(WeatherDetails : WeatherCodable){
        self.wearherModel = WeatherDetails
        self.responseData?()
    }
}
