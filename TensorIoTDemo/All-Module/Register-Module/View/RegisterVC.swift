//
//  RegisterVC.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage

class RegisterVC: UIViewController {
    
    //MARK: - PROPERTIES
    @IBOutlet var registerTableView : UITableView!
    var imagePicker = UIImagePickerController()
    var viewModel = {
        RegisterVM()
    }()
    private let database = Database.database().reference()
    
    
    //MARK: - LIFE CYCLE OF VIEW CONTROLLER
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - MOVE TO PREVIOUS PAGE
    @IBAction func backButtonAction(){
        self.POP()
    }
    
    //MARK: - SIGN UP ACTION
    @IBAction func signUpAction(){
        self.viewModel.validation(isImageUpload: getRegistorDatails().0, email: getRegistorDatails().1, password: getRegistorDatails().2, confirmPassword: getRegistorDatails().3, userName: getRegistorDatails().4, shortBio: getRegistorDatails().5) { message, sucess in
            if sucess {
                self.registerOnFirebase()
            }else{
                self.showAlert(title: .kAlert, message: message)
            }
        }
    }
    
    //MARK: - UPLOAD MEDIA
    func uploadMedia(completion: @escaping (_ url: String?) -> Void) {
        let indexPath0 = IndexPath(row: 0, section: 0)
        let cell0 = self.registerTableView.cellForRow(at: indexPath0) as! ProfileCell
        
        let storageRef = Storage.storage().reference().child("Avatar\(Int.random(in: 0..<10000)).png")
        if let uploadData = cell0.profileImage.resized(to: CGSize(width: 100, height: 100)).pngData() {
            storageRef.putData(uploadData) { (metadata, error) in
                if error != nil {
                    self.showAlert(title: .kAlert, message: Message.shared.failedToPush)
                    completion(nil)
                }else{
                    storageRef.downloadURL { url, error in
                        if error != nil {
                            self.showAlert(title: .kAlert, message: Message.shared.failedToPush)
                            completion(nil)
                            return
                        }else{
                            completion(url?.absoluteString)
                        }
                    }
                }
            }
        }
    }
        
    //MARK: - REGISTER ON FIREBASE
    func registerOnFirebase(){
        LoadingOverlay.shared.showOverlay(view: self.view)
        uploadMedia() { url in
            guard let url = url else { return }
            self.database.child("Register").childByAutoId().setValue([
                "imageUrl" : url,
                "email"    : self.getRegistorDatails().1,
                "password" : self.getRegistorDatails().2,
                "userName" : self.getRegistorDatails().4,
                "shortBio" : self.getRegistorDatails().5
            ])
            self.showAlert(isRegister: true,title: .kAlert, message: Message.shared.registerSuccess)
            LoadingOverlay.shared.hideOverlayView()
        }
    }
    
    
    //MARK: - GET REGISTOR DETAILS 
    func getRegistorDatails()->(String,String,String,String,String,String){
        let indexPath0 = IndexPath(row: 0, section: 0)
        let cell0 = self.registerTableView.cellForRow(at: indexPath0) as! ProfileCell
        let isProfileUpload = cell0.profileImage.size.width == 0 ? .kEmpty : "PROFILE_UPLOADED"
       
        let indexPath1 = IndexPath(row: 1, section: 0)
        let cell1 = self.registerTableView.cellForRow(at: indexPath1) as! RegisterCell
        
        let indexPath2 = IndexPath(row: 2, section: 0)
        let cell2 = self.registerTableView.cellForRow(at: indexPath2) as! RegisterCell
        
        let indexPath3 = IndexPath(row: 3, section: 0)
        let cell3 = self.registerTableView.cellForRow(at: indexPath3) as! RegisterCell
        
        let indexPath4 = IndexPath(row: 4, section: 0)
        let cell4 = self.registerTableView.cellForRow(at: indexPath4) as! RegisterCell
        
        let indexPath5 = IndexPath(row: 5, section: 0)
        let cell5 = self.registerTableView.cellForRow(at: indexPath5) as! RegisterCell
        
        return (isProfileUpload,cell1.textField.text!,cell2.textField.text!,cell3.textField.text!,cell4.textField.text!,cell5.textField.text!)
    }
}

