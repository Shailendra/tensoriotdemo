//
//  RegistorImagePicker.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation
import UIKit

//===================================================================================
//MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate
//===================================================================================
extension RegisterVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            let indexPath = IndexPath(row: 0, section: 0)
            let cell = self.registerTableView.cellForRow(at: indexPath) as! ProfileCell
            cell.profileImage = pickedImage
            self.registerTableView.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension RegisterVC {
    
    //MARK: - SELECT CAMERA ACTION
    @objc func SelectCameraAction(){
        let alert = UIAlertController(title: .kAvatar, message: Message.shared.selectAnOption, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: .kPhoto, style: .default , handler:{ (UIAlertAction)in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction(title: .kCamera, style: .default , handler:{ (UIAlertAction)in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: .kCancel, style: .cancel, handler:{ (UIAlertAction)in}))
        self.present(alert, animated: true, completion: {})
    }
    
    //MARK: - OPEN THE CAMERA
    func openCamera(){
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.imagePicker.allowsEditing = false
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        else{
            let alert  = UIAlertController(title: .kWarning, message: Message.shared.dontHaveCamera, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: .kOK, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - OPEN THE GALLARY
    func openGallary(){
        self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.imagePicker.allowsEditing = false
        self.imagePicker.delegate = self
        self.present(self.imagePicker, animated: true, completion: nil)
    }
}
