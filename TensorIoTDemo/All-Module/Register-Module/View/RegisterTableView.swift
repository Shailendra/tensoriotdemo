//
//  RegisterTableView.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation
import UIKit


//==============================================================
//MARK: -  UITableViewDelegate,UITableViewDataSource
//==============================================================
extension RegisterVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier:ProfileCell.identifier, for: indexPath) as!
            ProfileCell
            cell.cameraButton.addTarget(self, action: #selector(SelectCameraAction), for: .touchUpInside)
            cell.profileImageView.contentMode = .scaleToFill
            if cell.profileImage.size.width == 0 {
                cell.profileImageView.image = UIImage(named:ImageName.kAvator)
            }else{
                cell.profileImageView.image = cell.profileImage
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:CellName.kEmailCell, for: indexPath) as!
            RegisterCell
            cell.textField.placeholder = .kEmail
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier:CellName.kPasswordCell, for: indexPath) as! RegisterCell
            cell.textField.placeholder = .kPassword
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier:CellName.kConfirmPasswordCell, for: indexPath) as! RegisterCell
            cell.textField.placeholder = .kConfirmPassword
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier:CellName.kUserNameCell, for: indexPath) as! RegisterCell
            cell.textField.placeholder = .kUserName
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier:CellName.kBioCell, for: indexPath) as!
            RegisterCell
            cell.textField.placeholder = .kAShortBio
            return cell
        default: break
        }
        return UITableViewCell()
    }
}
