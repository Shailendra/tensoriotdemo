//
//  ProfileCell.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import UIKit

class ProfileCell: UITableViewCell {

    //MARK: - PROPERTIES
    @IBOutlet var cameraButton     : UIButton!
    @IBOutlet var profileImageView : UIImageView!
    var profileImage               = UIImage()
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
