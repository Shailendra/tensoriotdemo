//
//  RegisterCell.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import UIKit

class RegisterCell: UITableViewCell {

    //MARK: - PROPERTIES
    @IBOutlet var textField : UITextField!
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
