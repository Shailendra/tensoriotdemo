//
//  RegisterVM.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation

class RegisterVM  {
    
    func validation(isImageUpload : String, email : String, password:String,confirmPassword:String,userName:String,shortBio:String,completion: @escaping (_ message : String, _ sucess: Bool) -> Void) {
        if isImageUpload.isEmpty {
            completion(Message.shared.uploadProfileImage, false)
        }else if email.isEmpty {
            completion(Message.shared.enterEmailID, false)
        }else if email.isValidEmail() == false {
            completion(Message.shared.validEmailID, false)
        }else if password.isEmpty {
            completion(Message.shared.enterPassword, false)
        }else if confirmPassword.isEmpty {
            completion(Message.shared.enterConfirmPassword, false)
        }else if password != confirmPassword {
            completion(Message.shared.samePassword, false)
        }else if userName.isEmpty {
            completion(Message.shared.enterUserName, false)
        }else if shortBio.isEmpty {
            completion(Message.shared.enterShortBio, false)
        }else{
            completion(.kEmpty, true)
        }
    }
}
