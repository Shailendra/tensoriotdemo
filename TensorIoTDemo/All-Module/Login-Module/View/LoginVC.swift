//
//  ViewController.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import UIKit
import FirebaseDatabase

class ViewController: UIViewController {
    
    //MARK: - PROPERTIES
    @IBOutlet weak var emailTextField    : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!
    var viewModel = {
        LoginVM()
    }()
    private let database = Database.database().reference()
    
    
    //MARK: - LIFE CYCLE OF VIEW CONTROLLER
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginButtonAction(){
        self.viewModel.validation(email: self.emailTextField.text!, password: self.passwordTextField.text!) { message, sucess in
            if sucess {
                self.getRecordFromFirebase()
            }else{
                self.showAlert(title: .kAlert, message: message)
            }
        }
    }
    
    func getRecordFromFirebase(){
        self.database.child("Register").observeSingleEvent(of: .value) { snapshot,arg  in
            guard let value = snapshot.value as? [String: [String: String]] else{
                return
            }
            
            let targetEmail    = self.emailTextField.text!
            let targetPassword = "\(self.passwordTextField.text!)"

            var foundEmail    : String?
            var foundPassword : String?
            var userName      : String?
            var shortBio      : String?
            var imageUrl      : String?
            
            for (_, userData) in value {
                if let email = userData["email"], let password = userData["password"] {
                    if email.lowercased() == targetEmail.lowercased() && password.lowercased() == targetPassword.lowercased() {
                        foundEmail = email
                        foundPassword = password
                        userName = userData["userName"]
                        shortBio = userData["shortBio"]
                        imageUrl = userData["imageUrl"]
                        break
                    }
                }
            }

            if foundEmail?.lowercased() == self.emailTextField.text!.lowercased() && foundPassword?.lowercased() == "\(self.passwordTextField.text!.lowercased())" {
                removeAllKeys()
                self.MoveToProfileScreen(userName: userName!, shortBio: shortBio!,imageUrl: imageUrl!)
            }else{
                self.showAlert(title: .kAlert, message: "Please enter correct email id and password")
            }
        }
    }
}
