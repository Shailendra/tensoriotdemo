//
//  LoginVM.swift
//  TensorIoTDemo
//
//  Created by Shailendra Kumar Gupta on 26/09/23.
//

import Foundation

class LoginVM : NSObject {
    
    func validation(email : String, password:String,completion: @escaping (_ message : String, _ sucess: Bool) -> Void) {
        if email.isEmpty {
            completion(Message.shared.enterEmailID, false)
        }else if email.isValidEmail() == false {
            completion(Message.shared.validEmailID, false)
        }else if password.isEmpty {
            completion(Message.shared.enterPassword, false)
        }else{
            completion(.kEmpty, true)
        }
    }
}

